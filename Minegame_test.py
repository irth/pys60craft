from appuifw import *
import e32
from graphics import *
from key_codes import *
import random
import time
import math

log=Text()


healtimer=e32.Ao_timer()
log=Text()
def debug():#cleaned
    app.body=log
    app.exit_key_handler=lock.signal
        

def redrawInv(arg):#cleaned
    buffer.rectangle((1*16,1*16,14*16,18*16),(0,0,0),fill=(170,170,170))
    i=1
    x=1
    z=1
    
    while i<BlockCount:
        if Inventory[i]!=0:
            if x==12 and z==1:
                    z=2
                    x=1            
            elif x==12 and z==2:
                    z=3
                    x=1
            elif x==12 and z==3:
                    z=4
                    x=1
            elif x==12 and z==4:
                    z=5
                    x=1
            elif x==12 and z==5:
                    z=6
                    x=1
            elif x==12 and z==6:
                    z=7
                    x=1
            elif x==12 and z==7:
                    z=8
                    x=1
            elif x==12 and z==8:
                    z=9
                    x=1
            elif x==12 and z==9:
                    z=10
                    x=1
            x0=z*16+z*8
            y0=x*16+x*8
            buffer.blit(BlockImg[i],target=(x0,y0))
            buffer.text((x0+2,y0+10),unicode(str(Inventory[i])),(255,255,255))
            x=x+1
        i=i+1
    gfx.blit(buffer)
    
    

def candestroy(a):#cleaned
    r=1
    if a==3:
        r=0
    elif a==0:
        r=0
    elif a==4:
        r=0
    elif a==5:
        r=0
    elif a==6:
        r=0
    return r

def canplace(x,y):
    if CursorX==PlayerX:
        if CursorY==PlayerY:
            return 0
        if CursorY==PlayerY+1:
            return 0
    btype=BlockType[World[x][y]]
    if btype!=0 and btype!=5:
         return 0
    return 1;

def select():#cleaned
    global OffsetX
    global CursorX
    global CursorY
    x=OffsetX+CursorX
    y=CursorY+OffsetY
    if ActiveBlock==0 or CursorActive==0:
        return  
    if canplace(x,y)==0:
        return
        
    a=Inventory[ActiveBlock]
    if a>0: 
        World[x][y]=ActiveBlock
        Inventory[ActiveBlock]=a-1
        if a==1:
            nextblock()
        else:
            redraw(())
    else: nextblock()
    

def nextblock(): #cleaned
    global ActiveBlock
    b=BlockCount
    a=ActiveBlock
    if ActiveBlock==BlockCount-1:
        a=0
    i=a+1
    if ActiveBlock==BlockCount-1:
        a=0
    c=0
    while(i<b):
        if Inventory[i]>0:
            c=i
            i=b+1
        else:
            i=i+1
    ActiveBlock=c
    redraw(())        
    

def prevblock(): #cleaned
    global ActiveBlock
    i=ActiveBlock-1
    if ActiveBlock==1:
        i=BlockCount-1
    c=0
    while i>0:
        if Inventory[i]>0:
            c=i
            i=0
        else:
            i=i+1;
    ActiveBlock=c
    redraw(())        
    
def rightsoftkey():#cleaned
    global CursorActive
    global PlayerY
    global OffsetX
    global OffsetY
    if CursorActive:   
        x=CursorX+OffsetX
        y=CursorY+OffsetY
        a=BlockType[World[x][y]]
        z=World[x][y]
        if candestroy(a):
            Inventory[z]=Inventory[z]+1
            World[x][y]=checkdestroy(x,y,z)
            redraw(())
            if ActiveBlock==0:
                nextblock()
            moveplayer(3)
def interpolate(a,b,x):#cleaned
    c=(1-math.cos(x*math.pi))/2
    return round(a*(1-c)+b*c)
    
def generate():#cleaned
    gfx.text((10,45),u'Generating terrain...', 0xFFFFFF)
    #walls
    for y in range(40):
        World[7][y]=2
        World[143][y]=2
    
    #dirt
    worldmin=22
    worldmax=10
    heightmap=[0]*151
    
    for i in range(0,16):
        A=i*10
        heightmap[A]=random.choice(range(worldmax,worldmin))
    for i in range(0,15):
        start=i*10+1
        end=start+8
        a=heightmap[start-1]
        b=heightmap[end+1]
        x=0.1
        y=start
        while y<=end:
            heightmap[y]=interpolate(a,b,x)
            x=x+0.1
            y=y+1
    for x in range(8,143):
        a=heightmap[x]
        
        for y in range(a,39):
            World[x][y]=1

    
    h=16
    for x in range(8,143):
        for y in range(h,heightmap[x]):
            World[x][y]=3
    for x in range(8,143):
        if World[x][h]==3:
            World[x][h]=4
    for x in range(8,143):
        if heightmap[x]<=h:
            World[x][float.__int__(heightmap[x]+0.0)]=5
    for x in range(7,144):
        World[x][39]=2
    
def movecursor(a): #cleaned  
    global CursorX
    global CursorY
    # 0-left 1-right 2-up 3-down
    if a==0 and CursorX>0:
            CursorX=CursorX-1
    if a==1 and CursorX<14:
            CursorX=CursorX+1
    if a==2 and CursorY>0:
            CursorY=CursorY-1
    if a==3 and CursorY<19:
            CursorY=CursorY+1
    redraw(0)
     
def canmoveto(t,u): #cleaned
    ta=0
    ua=0
    if t==0 or t==2 or t==5:
        ta=1
    if u==2 or u==5 or u==0:
        ua=1
    if ua==1 and ta==1:
            return 1
    return 0


jump=0
    
def checkdestroy(x,y,a): #cleaned
    ret=0
    left=0
    right=0
    up=0
    down=0
    if y<19:
        down=World[x][y+1]
    if y>0:
        up=World[x][y-1]
    left=World[x-1][y]
    right=World[x+1][y]
    if left==3 or right==3:
        ret=3
    if left==4 or right==4:
        ret=4
    if up==3 or up==4:
        ret=3
    return ret
    
def moveplayer(a):#cleaned
    global healtimer
    global PlayerLife
    global OffsetX
    global OffsetY
    global PlayerY
    global PlayerX
    global jump
    r=0          
    StartY=0
    if a==3:r=1
    if a==0:
        b=BlockType[ScreenWorld[PlayerX-1][PlayerY]]
        c=BlockType[ScreenWorld[PlayerX-1][PlayerY+1]]
        if canmoveto(b,c):
            r=1
            OffsetX=OffsetX-1
    elif a==1:
        b=BlockType[ScreenWorld[PlayerX+1][PlayerY]]
        c=BlockType[ScreenWorld[PlayerX+1][PlayerY+1]]
        if canmoveto(b,c):
            r=1
            OffsetX=OffsetX+1
    elif a==2:
        if PlayerY+OffsetY>0:
            b=BlockType[ScreenWorld[PlayerX][PlayerY-1]]
            if canmoveto(b,b):
                if jump==0:
                    r=1
                    if PlayerY+OffsetY>10 and OffsetY+PlayerY<26:
                        OffsetY=OffsetY-1
                    else:
                        PlayerY=PlayerY-1
                    jump=1
                    screenworldupd()
                    redraw(())
                    e32.ao_sleep(0.5)
            jump=0
    
    StartY=PlayerY
    redraw(())
    if r==1:
        b=BlockType[ScreenWorld[PlayerX][PlayerY+1]]
        c=BlockType[ScreenWorld[PlayerX][PlayerY+2]]
        while canmoveto(b,c):
            py=OffsetY+PlayerY
            if py>9 and py<25:
                OffsetY=OffsetY+1
            else:
                PlayerY=PlayerY+1;
            redraw(())
            b=BlockType[ScreenWorld[PlayerX][PlayerY+1]]
            c=BlockType[ScreenWorld[PlayerX][PlayerY+2]]
        healtimer.cancel()
        healtimer.after(5,heal)
        if PlayerY-StartY>2:
            PlayerLife=PlayerLife-1
        if PlayerY-StartY>4:
            PlayerLife=PlayerLife-1
        if PlayerY-StartY>6:
            PlayerLife=PlayerLife-1
        if PlayerY-StartY>8:
            PlayerLife=PlayerLife-2
        if PlayerY-StartY>10:
            PlayerLife=PlayerLife-3
        if PlayerLife<1:
            note(u'Game over')
            lock.signal()
    redraw(0)

def automask(a): #cleaned
    b,c=a.size;
    d=Image.new(a.size,'1');
    e=a.getpixel((0,0))[0];
    for y in range(c):
        line=a.getpixel([(x,y) for x in range(b)]);
        for x in range(b):
            if line[x]==e:
                d.point((x,y),0)
    return d
    
def resize(arg):#cleaned
    return
    
def event(arg):#cleaned
    return
    
def redrawGame(arg):#cleaned
    draw_world()
    draw_player()
    draw_gui()   
    gfx.blit(buffer)

def redraw(arg):#cleaned
    return

def BlockImgLoad(a):#clnd
    b=Image.new((16,16))
    b.load(a)
    return b
    
def draw_world():#clnd
    global buffer
    screenworldupd()
    for x in range(0,15):
        for y in range(0,20):
            img=BlockImg[ScreenWorld[x][y]]
            buffer.blit(img, target=(x*16,y*16))
    
            
def draw_player():
    global PlayerEyes
    global CursorX
    global buffer
    if CursorX<=PlayerX:
        PlayerEyes=1
    else:
        PlayerEyes=0
    if PlayerEyes==0:
        buffer.blit(PlayerImgR, target=(PlayerX*16, PlayerY*16), mask=PlayerMaskR)
    if PlayerEyes==1:
        buffer.blit(PlayerImgL, target=(PlayerX*16, PlayerY*16), mask=PlayerMaskL)

    
def draw_gui():
    global buffer
    global CursorX
    global CursorY
    global CursorActive
    global PlayerX
    global PlayerY
    global ActiveBlock
     
    CursorActive=0
    if PlayerX-CursorX>-3:
        if PlayerX-CursorX<3:
            if PlayerY-CursorY>-4:
                if PlayerY-CursorY<3:
                    CursorActive=1
     
    hx=2
    hy=2
    ha=2
    for i in range(0,PlayerLife):
        buffer.blit(heart, target=(hx+i*18+ha*i, hy), mask=heartmask)
    
    a=(50,50,50) 
    if CursorActive:
        a=(255,0,0)
    buffer.rectangle((CursorX*16,CursorY*16, CursorX*16+16,CursorY*16+16), a)
    buffer.blit(BlockImg[ActiveBlock],target=(14*16-3,3))
    if ActiveBlock!=0:
        buffer.text((14*16-2,14), unicode(str(Inventory[ActiveBlock])), 0xFFFFFF)
    buffer.rectangle((14*16-4, 2, 14*16-4+17, 2+17), 0x00FFFF)
     
def game():
    global PlayerY
    global PlayerX
    PlayerY=0
    moveplayer(3)
    heal()
    heal()
    redraw(())


def modeInv():
    global redraw
    redraw=redrawInv
    gfx.bind(EKeyLeftArrow, None)
    gfx.bind(EKeyRightArrow, None)
    gfx.bind(EKeyUpArrow, None)
    gfx.bind(EKeyDownArrow, None)
    gfx.bind(EKey4, None)
    gfx.bind(EKey6, None)
    gfx.bind(EKey2, None)
    gfx.bind(EKey8, None)
    gfx.bind(EKeyStar, None)
    gfx.bind(EKeyHash, None)
    gfx.bind(EKeySelect, None)
    gfx.bind(EKey0, modeGame)
    app.exit_key_handler=nothing
    redraw(())

def nothing():
    return


def modeGame():
    global redraw
    redraw=redrawGame
    gfx.bind(EKeyLeftArrow, lambda:movecursor(0))
    gfx.bind(EKeyRightArrow, lambda:movecursor(1))
    gfx.bind(EKeyUpArrow, lambda:movecursor(2))
    gfx.bind(EKeyDownArrow, lambda:movecursor(3))
    gfx.bind(EKey4, lambda:moveplayer(0))
    gfx.bind(EKey6, lambda:moveplayer(1))
    gfx.bind(EKey2, lambda:moveplayer(2))
    gfx.bind(EKey8, lambda:moveplayer(3))
    gfx.bind(EKeyStar, debug)
    gfx.bind(EKeyHash, lock.signal)
    gfx.bind(EKeySelect, select)
    gfx.bind(EKey0, modeInv)
    gfx.bind(EKey3, nextblock)
    gfx.bind(EKey1, prevblock)
    app.exit_key_handler=rightsoftkey
    redraw(())


gfx=Canvas(redraw_callback=redraw, event_callback=event, resize_callback=resize)

lock=e32.Ao_lock()
app.menu=[]
app.screen='full'
app.body=gfx
app.orientation='portrait'
app.title=u'MineGame'


buffer=Image.new((240,320))

gfx.clear(0)
gfx.text((10,15),u'Loading blocks...', 0xFFFFFF)

ActiveBlock=1

#Max 90 blok
BlockCount=6
Block=[u'']*BlockCount
BlockType=[0]*BlockCount
# 0air 1solid 2doors 3adminium 4special 5watrtop 6watr 7lavatop 8lava
BlockImg=[0]*BlockCount
Inventory=[1]*BlockCount


Block[0]=u'Air'
BlockImg[0]=BlockImgLoad(u'C:\\Data\\MG\\blocks\\Air.png')
BlockType[0]=0

Block[1]=u'Dirt'
BlockImg[1]=BlockImgLoad(u'C:\\Data\\MG\\blocks\\Dirt.png')
BlockType[1]=1

Block[2]=u'Bedrock'
BlockImg[2]=BlockImgLoad(u'C:\\Data\\MG\\blocks\\Bedrock.png')
BlockType[2]=3


Block[3]=u'Water0'
BlockImg[3]=BlockImgLoad(u'C:\\Data\\MG\\blocks\\Water.png')
BlockType[3]=6

Block[4]=u'Water1'
BlockImg[4]=BlockImgLoad(u'C:\\Data\\MG\\blocks\\Water.png')
BlockType[4]=5

Block[5]=u'Grass'
BlockImg[5]=BlockImgLoad(u'C:\\Data\\MG\\blocks\\Grass.png')
BlockType[5]=1

########


gfx.text((10,30),u'Loading player...', 0xFFFFFF)
# player, cursor
CursorX=7
CursorY=11
CursorActive=0

OffsetX=100
PlayerX=7
PlayerY=11

PlayerEyes=1



PlayerImgR=Image.new((16,32))
PlayerImgR.load(u'C:\\Data\\MG\\mob\\char.png')
PlayerImgL=PlayerImgR.transpose(FLIP_LEFT_RIGHT)
PlayerMaskR=automask(PlayerImgR)
PlayerMaskL=PlayerMaskR.transpose(FLIP_LEFT_RIGHT)
    
PlayerLife=5
heart=Image.new((18,18))
heart.load(u'C:\\Data\\MG\\icons\\heart.png')
heartmask=automask(heart)        

PlayerFood=5
food=Image.new((18,18))
food.load(u'C:\\Data\\MG\\icons\\food.png')
foodmask=automask(food)        
MaxOffsetY=19
OffsetY=5
World=[[0 for col in  range(40)] for  row in range(15*10)]
generate()

ScreenWorld=[[0 for col in  range(20)] for  row in range(15)]
def screenworldupd():
    global OffsetX
    for x in range(15):
        for y in range(20):
            x0=x+OffsetX
            y0=y+OffsetY
            ScreenWorld[x][y]=World[x0][y0]




modeGame()


def heal():
    global healtimer
    global PlayerLife
    if PlayerLife<10:
        PlayerLife=PlayerLife+1
    redraw(())
    healtimer.cancel()
    healtimer.after(5,heal)


healtimer.after(5,heal)
game();


lock.wait()
